/* Copyright (c) 2010 IPB, INRIA & CNRS
**
** This file originally comes from the Scotch software package for
** static mapping, graph partitioning and sparse matrix ordering.
**
** This software is covered by the BSD-2-Clause license :
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
** COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
** STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
** OF THE POSSIBILITY OF SUCH DAMAGE.
**
** The fact that you are presently reading this means that you have
** had knowledge of the BSD-2-Clause license and that you accept its
** terms.
*/
/************************************************************/
/**                                                        **/
/**   NAME       : fibo.c                                  **/
/**                                                        **/
/**   AUTHOR     : Francois PELLEGRINI                     **/
/**                                                        **/
/**   FUNCTION   : This module handles Fibonacci heaps.    **/
/**                                                        **/
/**   DATES      : # Version 1.0  : from : 01 may 2010     **/
/**                                 to     12 may 2010     **/
/**                                                        **/
/************************************************************/

/*
**  The defines and includes.
*/

#define FIBO

#include <malloc.h>
#include <memory.h>
#include <stdio.h>
#include "fibo.h"

/* Helper macros which can be redefined at compile time. */

#ifndef FIBO_INT
#define FIBO_INT                    int           /* "long long" can be used on 64-bit systems */
#endif /* FIBO_INT */

#ifndef errorPrint
#define errorPrint(s)               fprintf (stderr, s)
#endif /* errorPrint */

#ifndef memAlloc
#define memAlloc                    malloc
#define memSet                      memset
#define memFree                     free
#endif /* memAlloc */

/*********************************************/
/*                                           */
/* These routines deal with Fibonacci heaps. */
/*                                           */
/*********************************************/

/* This routine initializes a Fibonacci
** heap structure.
** It returns:
** - 0   : in case of success.
** - !0  : on error.
*/

int
fiboHeapInit (
FiboHeap * const            heapptr,
int                      (* cmpfptr) (const FiboNode * const, const FiboNode * const))
{
  if ((heapptr->degrtab = (FiboNode **) memAlloc ((sizeof (FIBO_INT) << 3) * sizeof (FiboNode *))) == NULL) /* As many cells as there are bits in a FIBO_INT */
    return (1);

  memSet (heapptr->degrtab, 0, (sizeof (FIBO_INT) << 3) * sizeof (FiboNode *)); /* Make degree array ready for consolidation: all cells set to NULL */

  heapptr->rootdat.linkdat.prevptr =              /* Link root node to itself */
  heapptr->rootdat.linkdat.nextptr = &heapptr->rootdat;
  heapptr->cmpfptr = cmpfptr;

  return (0);
}

/* This routine flushes the contents of
** the given Fibonacci heap.
** It returns:
** - VOID  : in all cases.
*/

void
fiboHeapExit (
FiboHeap * const            heapptr)
{
  if (heapptr->degrtab != NULL)
    memFree (heapptr->degrtab);
}

/* This routine flushes the contents of
** the given Fibonacci heap. It does not
** free any of its contents, but instead
** makes the heap structure look empty again.
** It returns:
** - VOID  : in all cases.
*/

void
fiboHeapFree (
FiboHeap * const            heapptr)
{
  heapptr->rootdat.linkdat.prevptr =              /* Link root node to itself */
  heapptr->rootdat.linkdat.nextptr = &heapptr->rootdat;
}

/* This routine perform the consolidation
** of roots per degree. It returns the best
** element found because this element is not
** recorded in the data structure itself.
** It returns:
** - !NULL  : pointer to best element found.
** - NULL   : Fibonacci heap is empty.
*/

FiboNode *
fiboHeapConsolidate (
FiboHeap * const            heapptr)
{
  FiboNode ** restrict  degrtab;
  int                   degrmax;
  int                   degrval;
  FiboNode *            rootptr;
  FiboNode *            nextptr;
  FiboNode *            bestptr;

  degrtab = heapptr->degrtab;

  for (rootptr = heapptr->rootdat.linkdat.nextptr, nextptr = rootptr->linkdat.nextptr, degrmax = 0; /* For all roots in root list */
       rootptr != &heapptr->rootdat; ) {
    degrval = rootptr->deflval >> 1;              /* Get degree, getting rid of flag part */
#ifdef FIBO_DEBUG
    if (degrval >= (sizeof (FIBO_INT) << 3))
      errorPrint ("fiboHeapConsolidate: invalid node degree");
#endif /* FIBO_DEBUG */
    if (degrtab[degrval] == NULL) {               /* If no heap with same degree already found */
      if (degrval > degrmax)                      /* Record highest degree found               */
        degrmax = degrval;

      degrtab[degrval] = rootptr;                 /* Record heap as first heap with this degree      */
      rootptr = nextptr;                          /* Process next root in list during next iteration */
      nextptr = rootptr->linkdat.nextptr;
    }
    else {
      FiboNode *            oldrptr;              /* Root which will no longer be a root */
      FiboNode *            chldptr;

      oldrptr = degrtab[degrval];                 /* Assume old root is worse           */
      if (heapptr->cmpfptr (oldrptr, rootptr) <= 0) { /* If old root is still better    */
        oldrptr = rootptr;                        /* This root will be be linked to it  */
        rootptr = degrtab[degrval];               /* We will go on processing this root */
      }

      degrtab[degrval] = NULL;                    /* Remaining root changes degree so leaves this cell */
      fiboHeapUnlink (oldrptr);                   /* Old root is no longer a root                      */
      oldrptr->deflval &= ~1;                     /* Whatever old root flag was, it is reset to 0      */
      oldrptr->pareptr = rootptr;                 /* Remaining root is now father of old root          */

      chldptr = rootptr->chldptr;                 /* Get first child of remaining root                                    */
      if (chldptr != NULL) {                      /* If remaining root had already some children, link old root with them */
        rootptr->deflval += 2;                    /* Increase degree by 1, that is, by 2 with left shift in deflval       */
        fiboHeapLinkAfter (chldptr, oldrptr);
      }
      else {                                      /* Old root becomes first child of remaining root */
        rootptr->deflval = 2;                     /* Real degree set to 1, and flag set to 0        */
        rootptr->chldptr = oldrptr;
        oldrptr->linkdat.prevptr =                /* Chain old root to oneself as only child */
        oldrptr->linkdat.nextptr = oldrptr;
      }
    }                                             /* Process again remaining root as its degree has changed */
  }

  bestptr = NULL;
  for (degrval = 0; degrval <= degrmax; degrval ++) {
    if (degrtab[degrval] != NULL) {               /* If some heap is found           */
      bestptr = degrtab[degrval];                 /* Record it as potential best     */
      degrtab[degrval] = NULL;                    /* Clean-up used part of array     */
      degrval ++;                                 /* Go on at next cell in next loop */
      break;
    }
  }
  for ( ; degrval <= degrmax; degrval ++) {       /* For remaining roots once a potential best root has been found */
    if (degrtab[degrval] != NULL) {
      if (heapptr->cmpfptr (degrtab[degrval], bestptr) < 0) /* If new root is better */
        bestptr = degrtab[degrval];               /* Record new root as best root    */
      degrtab[degrval] = NULL;                    /* Clean-up used part of array     */
    }
  }

  return (bestptr);
}

/* This routine returns the node of minimum
** key in the given heap. The node is searched
** for each time this routine is called, so this
** information should be recorded if needed.
** This is the non-macro version, for testing
** and setting up breakpoints.
** It returns:
** - !NULL  : pointer to best element found.
** - NULL   : Fibonacci heap is empty.
*/

#ifndef fiboHeapMin

FiboNode *
fiboHeapMin (
FiboHeap * const            heapptr)
{
  FiboNode *            bestptr;

  bestptr = fiboHeapMinMacro (heapptr);

#ifdef FIBO_DEBUG
  fiboHeapCheck (heapptr);
#endif /* FIBO_DEBUG */

  return (bestptr);
}

#endif /* fiboHeapMin */

/* This routine adds the given node to the
** given heap. This is the non-macro version,
** for testing and setting up breakpoints.
** It returns:
** - void  : in all cases.
*/

#ifndef fiboHeapAdd

void
fiboHeapAdd (
FiboHeap * const            heapptr,
FiboNode * const            nodeptr)
{
  fiboHeapAddMacro (heapptr, nodeptr);

#ifdef FIBO_DEBUG
  fiboHeapCheck (heapptr);
#endif /* FIBO_DEBUG */
}

#endif /* fiboHeapAdd */

/* This routine deletes the given node from
** the given heap, whatever ths node is (root
** or non root). This is the non-macro version,
** for testing and setting up breakpoints.
** It returns:
** - void  : in all cases.
*/

#ifndef fiboHeapDel

void
fiboHeapDel (
FiboHeap * const            heapptr,
FiboNode * const            nodeptr)
{
  fiboHeapDelMacro (heapptr, nodeptr);

#ifdef FIBO_DEBUG
  nodeptr->pareptr =
  nodeptr->chldptr =
  nodeptr->linkdat.prevptr =
  nodeptr->linkdat.nextptr = NULL;

  fiboHeapCheck (heapptr);
#endif /* FIBO_DEBUG */
}

#endif /* fiboHeapDel */

/* This routine checks the consistency of the
** given linked list.
** It returns:
** - !NULL  : pointer to the vertex.
** - NULL   : if no such vertex available.
*/

#ifdef FIBO_DEBUG

static
int
fiboHeapCheck2 (
const FiboNode * const      nodeptr)
{
  FiboNode *            chldptr;
  int                   degrval;

  degrval = 0;
  chldptr = nodeptr->chldptr;
  if (chldptr != NULL) {
    do {
      if (chldptr->linkdat.nextptr->linkdat.prevptr != chldptr) {
        errorPrint ("fiboHeapCheck: bad child linked list");
        return     (1);
      }

      if (chldptr->pareptr != nodeptr) {
        errorPrint ("fiboHeapCheck: bad child parent");
        return (1);
      }

      if (fiboHeapCheck2 (chldptr) != 0)
        return (1);

      degrval ++;
      chldptr = chldptr->linkdat.nextptr;
    } while (chldptr != nodeptr->chldptr);
  }

  if (degrval != (nodeptr->deflval >> 1)) {       /* Real node degree is obtained by discarding lowest bit */
    errorPrint ("fiboHeapCheck2: invalid child information");
    return     (1);
  }

  return (0);
}

int
fiboHeapCheck (
const FiboHeap * const      heapptr)
{
  FiboNode *            nodeptr;

  for (nodeptr = heapptr->rootdat.linkdat.nextptr;
       nodeptr != &heapptr->rootdat; nodeptr = nodeptr->linkdat.nextptr) {
    if (nodeptr->linkdat.nextptr->linkdat.prevptr != nodeptr) {
      errorPrint ("fiboHeapCheck: bad root linked list");
      return     (1);
    }

    if (nodeptr->pareptr != NULL) {
      errorPrint ("fiboHeapCheck: bad root parent");
      return (1);
    }

    if (fiboHeapCheck2 (nodeptr) != 0)
      return (1);
  }

  return (0);
}

#endif /* FIBO_DEBUG */
