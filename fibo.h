/* Copyright (c) 2010 IPB, INRIA & CNRS
**
** This file originally comes from the Scotch software package for
** static mapping, graph partitioning and sparse matrix ordering.
**
** This software is covered by the BSD-2-Clause license :
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
** COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
** STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
** OF THE POSSIBILITY OF SUCH DAMAGE.
**
** The fact that you are presently reading this means that you have
** had knowledge of the BSD-2-Clause license and that you accept its
** terms.
*/
/************************************************************/
/**                                                        **/
/**   NAME       : fibo.h                                  **/
/**                                                        **/
/**   AUTHOR     : Francois PELLEGRINI                     **/
/**                                                        **/
/**   FUNCTION   : This module contains the definitions of **/
/**                the generic Fibonacci heap.             **/
/**                                                        **/
/**   DATES      : # Version 1.0  : from : 01 may 2010     **/
/**                                 to     12 may 2010     **/
/**                                                        **/
/**   NOTES      : # Since this module has originally been **/
/**                  designed as a gain keeping data       **/
/**                  structure for local optimization      **/
/**                  algorithms, the computation of the    **/
/**                  best node is only done when actually  **/
/**                  searching for it.                     **/
/**                  This is most useful when many         **/
/**                  insertions and deletions can take     **/
/**                  place in the mean time. This is why   **/
/**                  this data structure does not keep     **/
/**                  track of the best node, unlike most   **/
/**                  implementations do.                   **/
/**                                                        **/
/************************************************************/

#ifndef FIBO_H
#define FIBO_H

/*
**  The type and structure definitions.
*/

/* The doubly linked list structure. */

typedef struct FiboLink_ {
  struct FiboNode_ *        prevptr;              /*+ Pointer to previous sibling element +*/
  struct FiboNode_ *        nextptr;              /*+ Pointer to next sibling element     +*/
} FiboLink;

/* The heap node data structure. The deflval
   variable merges degree and flag variables.
   The degree of a node is smaller than
   "bitsizeof (FIBO_INT)", so it can hold on
   an "int". The flag value is stored in the
   lowest bit of the value.                   */
   
typedef struct FiboNode_ {
  struct FiboNode_ *        pareptr;              /*+ Pointer to parent element, if any                +*/
  struct FiboNode_ *        chldptr;              /*+ Pointer to first child element, if any           +*/
  FiboLink                  linkdat;              /*+ Pointers to sibling elements                     +*/
  int                       deflval;              /*+ Lowest bit: flag value; other bits: degree value +*/
} FiboNode;

/* The heap data structure. The fake dummy node aims
   at handling root node insertion without any test.
   This is important as many insertions have to be
   performed.                                        */

typedef struct FiboHeap_ {
  FiboNode                  rootdat;              /*+ Dummy node for fast root insertion                      +*/
  FiboNode **               degrtab;              /*+ Consolidation array of size "bitsizeof (FIBO_INT)"      +*/
  int                    (* cmpfptr) (const FiboNode * const, const FiboNode * const); /*+ Comparison routine +*/
} FiboHeap;

/*
**  The macro definitions.
*/

/* This macro provides the reference to
   the user structure that contains the
   FiboNode structure, from a reference
   to some FiboNode retrieved from a
   FiboHeap structure.                  */

#define FIBO_DATA(p,s,f)            (((char *) (p)) - ((char *) (f)) + ((char *) (s)))

/* This is the core of the module. All of
   the algorithms have been de-recursived
   and written as macros.                 */

#define fiboHeapLinkAfter(o,n)      do {                              \
                                      FiboNode *        nextptr;      \
                                      nextptr = (o)->linkdat.nextptr; \
                                      (n)->linkdat.nextptr = nextptr; \
                                      (n)->linkdat.prevptr = (o);     \
                                      nextptr->linkdat.prevptr = (n); \
                                      (o)->linkdat.nextptr = (n);     \
                                    } while (0)

#define fiboHeapUnlink(n)           do {                                                            \
                                      (n)->linkdat.prevptr->linkdat.nextptr = (n)->linkdat.nextptr; \
                                      (n)->linkdat.nextptr->linkdat.prevptr = (n)->linkdat.prevptr; \
                                    } while (0)

#define fiboHeapAddMacro(t,n)       do {                                        \
                                      (n)->pareptr = NULL;                      \
                                      (n)->chldptr = NULL;                      \
                                      (n)->deflval = 0;                         \
                                      fiboHeapLinkAfter (&((t)->rootdat), (n)); \
                                    } while (0)

#define fiboHeapMinMacro(t)         (fiboHeapConsolidate (t))

#define fiboHeapCutChildren(t,n)    do {                                                \
                                      FiboNode *        chldptr;                        \
                                      chldptr = (n)->chldptr;                           \
                                      if (chldptr != NULL) {                            \
                                        FiboNode *        cendptr;                      \
                                        cendptr = chldptr;                              \
                                        do {                                            \
                                          FiboNode *        nextptr;                    \
                                          nextptr = chldptr->linkdat.nextptr;           \
                                          chldptr->pareptr = NULL;                      \
                                          fiboHeapLinkAfter (&((t)->rootdat), chldptr); \
                                          chldptr = nextptr;                            \
                                        } while (chldptr != cendptr);                   \
                                      }                                                 \
                                    } while (0)

#define fiboHeapDelMacro(t,n)       do {                                                    \
                                      FiboNode *        pareptr;                            \
                                      FiboNode *        rghtptr;                            \
                                      pareptr = (n)->pareptr;                               \
                                      fiboHeapUnlink (n);                                   \
                                      fiboHeapCutChildren ((t), (n));                       \
                                      if (pareptr == NULL)                                  \
                                        break;                                              \
                                      rghtptr = (n)->linkdat.nextptr;                       \
                                      while (1) {                                           \
                                        FiboNode *        gdpaptr;                          \
                                        int               deflval;                          \
                                        deflval = pareptr->deflval - 2;                     \
                                        pareptr->deflval = deflval | 1;                     \
                                        gdpaptr = pareptr->pareptr;                         \
                                        pareptr->chldptr = (deflval <= 1) ? NULL : rghtptr; \
                                        if (((deflval & 1) == 0) || (gdpaptr == NULL))      \
                                          break;                                            \
                                        rghtptr = pareptr->linkdat.nextptr;                 \
                                        fiboHeapUnlink (pareptr);                           \
                                        pareptr->pareptr = NULL;                            \
                                        fiboHeapLinkAfter (&((t)->rootdat), pareptr);       \
                                        pareptr = gdpaptr;                                  \
                                      }                                                     \
                                    } while (0)

/*
**  The function prototypes.
*/

/* This set of definitions allows the user
   to specify whether they prefer to use
   the fibonacci routines as macros or as
   regular functions, for instance for
   debugging.                              */

/* #define fiboHeapAdd              fiboHeapAddMacro */
/* #define fiboHeapDel              fiboHeapDelMacro */
/* #define fiboHeapMin              fiboHeapMinMacro */

int                         fiboHeapInit        (FiboHeap * const, int (*) (const FiboNode * const, const FiboNode * const));
void                        fiboHeapExit        (FiboHeap * const);
void                        fiboHeapFree        (FiboHeap * const);
FiboNode *                  fiboHeapConsolidate (FiboHeap * const);
#ifndef fiboHeapAdd
void                        fiboHeapAdd         (FiboHeap * const, FiboNode * const);
#endif /* fiboHeapAdd */
#ifndef fiboHeapDel
void                        fiboHeapDel         (FiboHeap * const, FiboNode * const);
#endif /* fiboHeapDel */
#ifndef fiboHeapMin
FiboNode *                  fiboHeapMin         (FiboHeap * const);
#endif /* fiboHeapMin */
#ifdef FIBO_DEBUG
int                         fiboHeapCheck       (const FiboHeap * const);
#ifdef FIBO
static int                  fiboHeapCheck2      (const FiboNode * const);
#endif /* FIBO       */
#endif /* FIBO_DEBUG */

#endif /* FIBO_H */
