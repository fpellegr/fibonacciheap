# FibonacciHeap

## Description

This small module provides an efficient and versatile implementation
in C of the _Fibonacci Heap_ (or _Fibonacci Trees_) data structure.
For maximum efficiency, most routines are de-recursived, and users
have full control on memory allocation regarding the objects to be
inserted into the data structure.

## Rationale

In the course of the development of
[Scotch](https://gitlab.inria.fr/scotch/scotch), I needed to sort
elements according to several criteria, which might not always be
single integer values. Consequently, the bucket sorting algorithms
that I used before were not adequate. After looking at the [Red-Black
tree](http://en.wikipedia.org/wiki/Red-black_tree) data structure, I
turned to the [Fibonacci
heap](http://en.wikipedia.org/wiki/Fibonacci_heap) (also called
« Fibonacci trees », since it is technically a forest of trees with
given degree properties), which better fitted my needs. Indeed, in the
context of graph partitioning, I planned to use this data structure to
select the best vertex to move from one part to another, depending on
the improvement such a move may bring to the current partition. Such
local optimisation algorithms are classically referred to as
« Fiduccia-Mattheyses » or « Kernighan-Lin », depending on the way
vertices are moved or swapped between parts. In such algorithms, once
a vertex of best gain value has been selected for moving, the gains of
all its neighbors have to be recomputed. Consequently, the data
structure will be subject to much more deletion and insertion
operations of « unsorted » arbitrary elements than to searches for,
and deletions of, elements of best gain value. By adding and removing
arbitrary vertices quickly (in constant time for addition), and by
postponing the expensive sorting operations up to the moment when
searching for the element of extremal value, the Fibonacci heap data
structure is just what I needed when buckets lists could not be used.

However, all of the existing free/libre implementations that I could
find on the Internet had two drawbacks. Firstly, all of them were
recursive, incurring a heavy recursion penalty for a few useful
instructions per function call. Secondly, all of them treated elements
as independent data structures, which were internally allocated and
freed at insertion and deletion time. Both of these features made them
completely inappropriate for the kind of heavy use that I had in mind,
for which such routines have to be called millions of times, and
vertex elements are already embodied within other data structures such
as hash table arrays (yes, high performance computing can be real fun
in terms of data structures!).

Consequently, I wrote my own, de-recursived, implementation of the
Fibonacci heap data structure. The code is small, as this is really a
_very_ elegant data structure. Save for the pointer array internally
used by the consolidation routine, no memory allocation is performed
by this module. The allocation and freeing of the individual elements
are left to the users of the module, who can opt for the kind of memory
handling that they want. Unlike most implementations, the element of
minimum value is not internally recorded, as I had no need to preserve
it; however, module users can easily implement this feature on top of
the existing routines. All module routines, except the consolidation
routine which computes the current extremal value, are written as
macros, which can be used as is or embodied within regular functions,
depending on the pre-definition of symbolic constants.

## Usage

The Fibonacci heap is handled by means of a `FiboHeap` data
structure. Elements to be inserted and searched for must comprise a
`FiboNode` substructure. Elements can belong to several heap
structures, provided they possess as many `FiboNode` substructures as
needed, and provided that users have a means for determining the
offset of the element structure from the reference to the `FiboNode`
that is returned by the module routines.

The module routines are the following:

- `fiboHeapInit` : Initializes a `FiboHeap` data structure, by
  allocating the internal consolidation array.
- `fiboHeapExit` : Destroys a `FiboHeap` data structure, by freeing
  the internal consolidation array. Heap elements are not modified in
  any way. If elements have to be individually processed at freeing
  time, for instance to be freed themselves, then they have to be
  removed one by one from the heap before `fiboHeapExit` is called, or
  be handled by means of external iterators (for instance, by
  traversing their support array if they were all allocated together).
- `fiboHeapFree` : Makes the `FiboHeap` data structure become free, by
  removing all of its elements. Like for `fiboHeapExit`, heap
  elements are not themselves considered nor modified in any way. Their
  chaining pointers just become meaningless, so that removing any
  previously inserted `FiboNode` element from a freed heap
  may lead to segmentation violation errors.
- `fiboHeapAdd` : Adds a `FiboNode` to a `FiboHeap`.
- `fiboHeapDel` : Removes the given `FiboNode` from its `FiboHeap`.
- `fiboHeapConsolidate` : Consolidates the current heap and returns a
  pointer to its extremal element. This element itself is not
  modified, and still belongs to the `FiboHeap`.
- `fiboHeapMin` : Returns the heap element of extremal value (assumed
  to be minimal, in my original context), without removing it from the
  `FiboHeap`. At the time being, this routine just calls
  `fiboHeapConsolidate`. Module users willing to keep track of the
  element of extremal gain can redefine this routine, as well as the
  `fiboHeapAdd` and `fiboHeapDel` routines, such that a pointer to the
  element of extremal gain is preserved in the `FiboHeap` data
  structure. The recorded element must be compared against elements
  which are removed and deleted, and updated whenever necessary (which
  may require a call to `fiboHeapConsolidate` if the extremal element
  is the one which is being removed from the `FiboHeap`).
- `fiboHeapCheck` : When the `FIBO_DEBUG` debugging flag has been set at
  compile time, this routine checks the consistency of the given
  `FiboHeap` structure.

This C implementation is meant to be very versatile. Consequently,
users have full control on memory allocation of the objects to be
inserted into the data structure. In order to help users handle
pointer conversions, the following macro is available:

- `FIBO_DATA` : Provides the reference to a user data structure, given
  the reference to the `FiboNode` field contained into this data
  structure and its offset within it.

## Usage and testing

In order to test the module and provide an example of use, a small
test program is available: `test_fibo.c`. It can be run by way of the
two following commands:

    gcc -Wall -pedantic -c fibo.c -o fibo.o
    gcc -Wall -pedantic test_fibo.c fibo.o -o test_fibo

## Support

I do not plan to provide any support for this code, but comments and
suggestions are welcome anyway.

## Authors and acknowledgment

François PELLEGRINI is the sole author of this code.

## License

This code is released under the BSD-2-Clause license.
