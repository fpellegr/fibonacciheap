/* Copyright (c) 2010 IPB, INRIA & CNRS
**
** This file originally comes from the Scotch software package for
** static mapping, graph partitioning and sparse matrix ordering.
**
** This software is covered by the BSD-2-Clause license :
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
** COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
** STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
** OF THE POSSIBILITY OF SUCH DAMAGE.
**
** The fact that you are presently reading this means that you have
** had knowledge of the BSD-2-Clause license and that you accept its
** terms.
*/
/************************************************************/
/**                                                        **/
/**   NAME       : fibo.h                                  **/
/**                                                        **/
/**   AUTHOR     : Francois PELLEGRINI                     **/
/**                                                        **/
/**   FUNCTION   : This module is a sample test program    **/
/**                for the generic Fibonacci heap module.  **/
/**                                                        **/
/**   DATES      : # Version 1.0  : from : 01 may 2010     **/
/**                                 to     12 may 2010     **/
/**                                                        **/
/************************************************************/

/*
**  The defines and includes.
*/

#define TEST_FIBO

#include <malloc.h>
#include <stdio.h>
#include <sys/time.h>

#include "fibo.h"

/*
**  The type and structure definitions.
*/

/* Sample data structure which uses the Fibonacci
   heap module to sort some of its properties.    */

typedef struct Data_ {
  double              dummy1;
  FiboNode            node;                       /* Can be everywhere in the data structure */
  int                 dummy2;
  int                 data;                       /* Can be everywhere in the data structure */
  float               dummy3;
} Data;

Data                  dummy;                      /* Dummy data for pointer offset computations */

#define DATA(p)                     ((Data *) FIBO_DATA ((p), &dummy, &dummy.node)) /* Retrieve reference to user objects from reference to node */

/* The comparison function which is used to sort
** "Data" structures. Since only "FiboNode" pointers
** are passed, pointer offset computations are
** necessary to access the whole "Data" structures,
** with all their fields. Since this Fibonacci heap
** implementation sorts data by ascending order,
** the comparison function must return a negative
** number if "node1" has to be returned before
** "node2" when extracting sorted data from the
** Fibonacci heap structure.
*/

static
int
cmpFunc (
const FiboNode * const      node0,
const FiboNode * const      node1)
{
  const Data * const  data0 = DATA (node0);
  const Data * const  data1 = DATA (node1);

/* Any kind of criteria can be used for sorting, provided
   that the comparison function guarantees a total order
   on the elements.                                       */

  return ((data0->data < data1->data) ? -1 : 1);
}

/* Sample insertion and extraction sequence. */

static int            sequence[] = { 2, 5, 12, 3, 6, -1, 2, 34, -1, -1, 6, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2 };

/*********************/
/*                   */
/* The main routine. */
/*                   */
/*********************/

int
main (
int                 argc,
char *              argv[])
{
  FiboHeap            heapdat;
  FiboNode *          nodeptr;
  Data *              dataptr;
  int                 i;

  fprintf (stderr, "Initializing heap\n");
  if (fiboHeapInit (&heapdat, cmpFunc) != 0) {
    fprintf (stderr, "Cannot initialize heap\n");
    return  (1);
  }

  for (i = 0; sequence[i] != -2; i ++) {
    if (sequence[i] >= 0) {

/* In this example, we allocate "Data" elements one
   by one by means of individual malloc's, but they
   could have been allocated as a whole as a single
   array of "Data" structures.                      */

      fprintf (stderr, "Inserting: \"%d\"\n", sequence[i]);
      dataptr = malloc (sizeof (Data));
      dataptr->data = sequence[i];
      fiboHeapAdd (&heapdat, &dataptr->node);
    }
    else {
      fprintf (stderr, "Getting min: ");
      nodeptr = fiboHeapMin (&heapdat);
      if (nodeptr == NULL)
        fprintf (stderr, "HEAP IS EMPTY\n");
      else {
        fiboHeapDel (&heapdat, nodeptr);

/* We always need this sort of data offset computations
   in order to get the pointer to the "Data" structure
   from the pointer to the "FiboNode" structure. A way
   to simplify it is to place the "FiboNode" structure
   at the very beginning of the "Data" structure. In
   this case, only a pointer type cast is required.     */

        dataptr = DATA (nodeptr);
        fprintf (stderr, "\"%d\"\n", dataptr->data);
        free    (dataptr);
      }
    }
  }

  fiboHeapExit (&heapdat);                        /* Necessary to free the internal consolidation array */

  return (0);
}
